---
title: NimbusFox's Staxel Mods
---

# What is Staxel?
Staxel is a pleasant voxel farming sandbox that let's you run your own farm at your leisure. There are no time constraints when it comes to quests and you are never at risk of losing your farm.

You can find this game at the following locations:

[PlayStaxel](https://playstaxel.com/)

[Steam](https://store.steampowered.com/app/405710/)

[Humble Bundle](https://www.humblebundle.com/store/staxel)

[GOG](https://www.gog.com/game/staxel)

[Discord](https://discord.gg/Staxel)

# What is the difference between a code mod and a content mod?
### Code mod
A code mod is where it brings new mechanics into the game and even possibly change vanilla mechanics. Most code mods are usually safe but it is recommended to be weary as some mod makers may have malicious intent.

### Content mod
Content mods only add blocks and items to the game and use vanilla mechanics. These are generally safe to install

# Why do I get taken to MonoGame documentation?
As FNA does not have a documentation site, to make things simpler it has been setup so the documentation of Microsoft.XNA.Framework is linked to MonoGame's documentation website. Please use this with a grain of salt as MonoGame is not the same as FNA. Staxel uses FNA 20.11
