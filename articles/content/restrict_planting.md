---
title: Restricting seed planting
---
# Restricting seed planting
By a request from toketsupuurin#2697 the feature to restrict what seeds can be planted on what dirt was created to allow for specialized dirt to only have certain seeds planted onto it. 

With a simple change to your plant and soil tiles you can restrict what seeds can be planted where.

# Prerequisites
<a href="https://wiki.playstaxel.com/Notification" target="_blank">Staxel Wiki - Notifications</a>

# Restricting planting from the seed side
Example:
```json
{
    "requiredSoil": {
        "soil": [
            "nimbusFox.kitsuneToolBox.dirt.testSoilTilled",
            "nimbusFox.kitsuneToolBox.dirt.testSoilWatered"
        ],
        "invalidSoilNotification": "nimbusFox.kitsuneToolBox.notifications.test"
    },
}
```
## More Details

```json
{
    "soil": [
        "nimbusFox.kitsuneToolBox.dirt.testSoilTilled",
        "nimbusFox.kitsuneToolBox.dirt.testSoilWatered"
    ]
}
```

`soil` is the array of strings that contain the soil to which the seeds can be planted on. This seed can only be planted on the soils specified. It's recommended to only specify the tilled and watered dirt tiles

```json
{
    "invalidSoilNotification": "nimbusFox.kitsuneToolBox.notifications.test"
}
```

`invalidSoilNotification` is used to specify what error notification is triggered should the seed try to be planted on a soil that is not supported.

You will need to create a `.notification` file that will have the information needed to send the player so they know they can't plant the seed on the soil.

# Restricting planting from the soil side
Example:
```json
{
    "seedSettings": {
        "allowAllSeeds": false,
        "invalidSeedNotification": "nimbusFox.kitsuneToolBox.notifications.test"
    }
}
```
## More Details
```json
{
    "allowAllSeeds": false
}
```
`allowAllSeeds` is used to define if any seeds can be used on it. If set to `true` any seed can be planted unless `requiredSoil` specifies otherwise in the plant config.

If set to `false` then only seeds that have the `requiredSoil` specify it's code can the seed be planted.

```json
{
    "invalidSeedNotification": "nimbusFox.kitsuneToolBox.notifications.test"
}
```
`invalidSeedNotification` is used to specify what error notification is triggered should the seed try to be planted on a soil that is not supported.

You will need to create a `.notification` file that will have the information needed to send the player so they know they can't plant the seed on the soil.