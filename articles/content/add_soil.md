---
title: Custom soils
---
# How to add more soils to Staxel
Thanks to replacing Staxel's patch system we can now inject new soil types into the game via it's `farming.config` file.

It is recommended to inherit from the vanilla dirt tiles. E.g.

```text
Custom dirt         => staxel/tile/dirt/Dirt.tile
Custom dirt tilled  => staxel/tile/dirt/Dirt_tilled.tile
Custom dirt watered => staxel/tile/dirt/Dirt_watered.tile
```
The reason for this is to make sure the necessary information is assigned to your dirt so they behave similarly to the vanilla dirt tiles.

Be sure to add your custom dirt to the tile drop else the player will be picking up dirt instead of your custom dirt.

Example:
```json
{
    "spawnOnBreak": {
        "drops": {
            "entries": [
                {
                    "kind": "staxel.item.Placer",
                    "tile": "nimbusFox.kitsuneToolBox.dirt.testSoil"
                }
            ]
        },
        "selfDropChance": 0.0
    }
}
```

Once you have created your custom soil you will need to use a `.patch` file to inject them into the `farming.config` file so that the game recognizes them as valid plantable soil.

Examples:

soilExample.tile
```json
{
    "__inherits": "staxel/tile/dirt/Dirt.tile",
    "code": "nimbusFox.kitsuneToolBox.dirt.testSoil"
}
```

soilExampleTilled.tile
```json
{
    "__inherits": "staxel/tile/dirt/Dirt_tilled.tile",
    "code": "nimbusFox.kitsuneToolBox.dirt.testSoilTilled",
    "spawnOnBreak": {
        "drops": {
            "entries": [
                {
                    "kind": "staxel.item.Placer",
                    "tile": "nimbusFox.kitsuneToolBox.dirt.testSoil"
                }
            ]
        },
        "selfDropChance": 0.0
    }
}
```

soilExampleWatered.tile
```json
{
    "__inherits": "staxel/tile/dirt/Dirt_watered.tile",
    "code": "nimbusFox.kitsuneToolBox.dirt.testSoilWatered",
    "spawnOnBreak": {
        "drops": {
            "entries": [
                {
                    "kind": "staxel.item.Placer",
                    "tile": "nimbusFox.kitsuneToolBox.dirt.testSoil"
                }
            ]
        },
        "selfDropChance": 0.0
    }
}
```

addSoil.patch
```json
{
    "file": "farming.config",
    "mergeLists": true,
    "patch": {
        "materials": {
            "nimbusFox.kitsuneToolBox.dirt.testSoil": {
                "tillable": true,
                "tilledMaterial": "nimbusFox.kitsuneToolBox.dirt.testSoilTilled"
            },
            "nimbusFox.kitsuneToolBox.dirt.testSoilTilled": {
                "tilled": true,
                "waterable": true,
                "wateredMaterial": "nimbusFox.kitsuneToolBox.dirt.testSoilWatered"
            },
            "nimbusFox.kitsuneToolBox.dirt.testSoilWatered": {
                "tilled": true,
                "watered": true,
                "unwateredMaterial": "nimbusFox.kitsuneToolBox.dirt.testSoilTilled"
            }
        }
    }
}
```

And that's it. You have now created your own valid dirt that the game will use for planting crops