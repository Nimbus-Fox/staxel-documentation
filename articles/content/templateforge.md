---
title: Template Forge
---
# Template Forge

The template forge is an easy way to mass produce content for the game without making the mod file stupidly large.

Say you want to introduce a wide range of colorful objects. Let's say around 20 colors and 100 items per color. That is a total of 20,000 items and the model sizes add up.

So to make things smaller you only need to make the 100 models and define the color changes in the template system and it will produce the the 20,000 items in memory for you.

[Template Forge](templateforge_forge.html)

[Template Cast](templateforge_cast.html)

[Template Material](templateforge_material.html)
