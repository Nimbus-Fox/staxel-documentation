# Introduction
All the tools available to content modders can be found to the left.

If you have any requests for tools that would make content modding even better.

Please let me know in Staxel's discord.

# Things to note
## Please read

The Kitsune ToolBox overrides the base game's patch system. 
  
What does it do instead? Any `.patch` file can now patch any json file in the content folder and not just what the game decides can be patched.
