---
title: Template Cast
---
# Prerequisites
<a href="https://wiki.playstaxel.com/Items" target="_blank">Staxel Wiki - Items</a>

<a href="https://wiki.playstaxel.com/Tile" target="_blank">Staxel Wiki - Tiles</a>

<a href="https://wiki.playstaxel.com/Reactions" target="_blank">Staxel Wiki - Reactions</a>

<a href="https://wiki.playstaxel.com/Recipes" target="_blank">Staxel Wiki - Recipes</a>


# Cast (`.kscast`)
The template cast is where you specify the model and contains some base json data to work from.

# Example
```json
{
    "code": "nimbusfox.nimbusTech.cast.metalOre",
    "builder": "nimbusfox.kitsuneToolBox.templateBuilder.items",
    "baseModel": "mods/NimbusTech/Staxel/OreTemplates/Ores/MetalOre.qb",
    "codeSuffix": ".ore",
    "template": {
        "categories": [
            "ore",
            "metalOre",
            "tech"
        ],
        "isDockable": true,
        "kind": "staxel.item.CraftItem",
        "usageOffset": {
            "x": 0.0,
            "y": 0.0,
            "z": 0.0
        },
        "inHandRotation": {
            "x": 0.0,
            "y": 0.0,
            "z": -1.56
        }
    },
    "translations": {
        "en-gb": {
            "name": "${name} Ore",
            "description": "A piece of stone with chunks of ${name:L} inside"
        }
    }
}
```

# More Detail
```json
{
    "code": "nimbusfox.nimbusTech.cast.metalOre"
}
```
The `code` is the identifier for the cast for you to reference in `.ksforge` files
```json
{
    "builder": "nimbusfox.kitsuneToolBox.templateBuilder.items"
}
```
The `builder` defines what builder to use for the item/block. You can specify a builder that currently does not exist and the generation will be ignored. This allows maximum flexibility and avoiding any errors that can occur due to missing mods

<a href="~/api/NimbusFox.KitsuneToolBox.TemplateForge.V1.Builders.html" target="_blank">Current builders included with Kitsune ToolBox</a>

```json
{
    "baseModel": "mods/NimbusTech/Staxel/OreTemplates/Ores/MetalOre.qb"
}
```
The `baseModel` defines what qb model to use and recolor based on what is specified in the `.ksmaterial` file it is "pouring" into the cast.

For the recoloring to occur you need to make sure all parts of the model being recolored must have it's RGB values the same. E.g. `R: 255 G: 255 B:255` The higher the color values the more intense the color will be painted onto the model
```json
{
    "codeSuffix": ".ore"
}
```
`codeSuffix` specifies what to put at the end of the code which is defined in `.ksmaterial` files
```json
{
    "template": {
        "categories": [
            "ore",
            "metalOre",
            "tech"
        ],
        "isDockable": true,
        "kind": "staxel.item.CraftItem",
        "usageOffset": {
            "x": 0.0,
            "y": 0.0,
            "z": 0.0
        },
        "inHandRotation": {
            "x": 0.0,
            "y": 0.0,
            "z": -1.56
        }
    }
}
```
The `template` section is to be treated like a normal `.tile`/`.item` file but keep in mind `.kscast` files can override values that share the same key as this file.
```json
{
    "translations": {
        "en-gb": {
            "name": "${name} Ore",
            "description": "A piece of stone with chunks of ${name:L} inside"
        }
    }
}
```
`translations` lets you define the translations so you don't need to fill `.lang` files guessing what might be created. `${}` allows you to call translation variables from `.ksmaterial` files. Specifying `:L` or `:U` after the variable name turns the variable output to lowercase and uppercase respectively. So if the `name` variable equals `Lava` doing `${name:L}` will turn it into `lava` and `${name:U}` will turn it into `LAVA`
