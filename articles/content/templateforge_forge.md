---
title: Template Forge
---
# Forge (`.ksforge`)
The template forge is what brings `.kscast` files and `.ksmaterial` files together.

It also allows you to add in global json depending on the cast.

# Example

```json
{
    "code": "nimbusfox.nimbusTech.forge.metals",
    "templates": {
        "nimbusfox.nimbusTech.cast.metalOre": true,
        "nimbusfox.nimbusTech.cast.oreChunk": {
            "kind": "staxel.item.CraftItem"
        }
    }
}
```

# More Detail

```json
{
    "code": "nimbusfox.nimbusTech.forge.metals"
}
```
The `code` is the identifier for the forge for you to reference in `.ksmaterial` files

```json
{
    "templates": {
        "nimbusfox.nimbusTech.cast.metalOre": true,
        "nimbusfox.nimbusTech.cast.oreChunk": {
            "kind": "staxel.item.CraftItem"
        }
    }
}
```
`templates` specify what casts to use for the material and what json data should be injected in

```json
{
    "nimbusfox.nimbusTech.cast.metalOre": true
}
```
Just specifying `true` for the value of the cast means it will use the cast and inject nothing else into the resulting json data. If you put `false` instead it will refuse to use that cast when creating items/blocks etc. This is helpful when inheriting a parent `.ksforge`

```json
{
    "nimbusfox.nimbusTech.cast.oreChunk": {
        "kind": "staxel.item.CraftItem"
    }
}
```
If you specify json data into the cast section then that will be included in the generated json data. This will also override the current one and will merge lists together. Helpful when you want a group of items/blocks to share certain characteristics
