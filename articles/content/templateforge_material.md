---
title: Template Material
---
# Material (`.ksmaterial`)

# Example
```json
{
    "code": "nimbusfox.nimbusTech.copper",
    "forge": "nimbusfox.nimbusTech.forge.metals",
    "color": "CC5B00",
    "template": {
        "categories": [
            "copper"
        ],
        "__inherits": [
            "mods/NimbusTech/Staxel/Ores/Metals/Copper/OreSpawn.json"
        ]
    },
    "translations": {
        "en-gb": {
            "name": "Copper"
        }
    }
}
```
# More Detail
```json
{
    "code": "nimbusfox.nimbusTech.copper"
}
```
`code` defines the starting code for the item/block etc.
```json
{
    "forge": "nimbusfox.nimbusTech.forge.metals"
}
```
`forge` defines the `.ksforge` file we'll be using to create our items/blocks
```json
{
    "color": "CC5B00"
}
```
`color` takes a hex value for color. This is what will be used when coloring models
```json
{
    "template": {
        "categories": [
            "copper"
        ],
        "__inherits": [
            "mods/NimbusTech/Staxel/Ores/Metals/Copper/OreSpawn.json"
        ]
    },
}
```
`template` is to be filled out like a basic json file 
```json
{
    "translations": {
        "en-gb": {
            "name": "Copper"
        }
    }
}
```
`translations` let you define translation variables to be used in `.kscast` files
