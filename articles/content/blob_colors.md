---
title: Colors in Blobs
---
# Colors in Blobs
The Kitsune ToolBox contains a handy function that allows code modders and content modders work with colors much easier.

There are 4 ways to store colors in the json files that the ToolBox supports

# 1. Object/Blob

# 2. Hexadecimal

# 3. Image

# 4. Color Int32