---
title: Kitsune ToolBox
---
![logo](~/images/logo.png)
# What is Kitsune ToolBox?
Kitsune ToolBox is a core mod for the voxel farming sandbox game known as Staxel.

It offers many powerful tools for both content modders and code modders that is not normally available from the base modding tools the game offers